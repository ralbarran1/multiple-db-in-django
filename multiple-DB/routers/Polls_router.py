class PollsRouter:

    route_app_labels = {'polls'}

    def db_for_read(self, model, **hint):
        if model._meta.app_label == 'polls':
            return 'Polls'
        return None

    def db_for_rite(self, model, **hint):
        if model._meta.app_label == 'polls':
            return 'Polls'
        return None

    def allow_relation(self, obj1, obj2, **hint):
        if (
            obj1._meta.app_label == 'polls' or
            obj2._meta.app_label == 'polls'
        ):
            return True
        return None

    def allow_migrates(self, db, app_label, model_name=None, **hints):
        if app_label == 'polls':
            return db == 'Polls'
        return None