class DevicesRouter:

    route_app_labels = {'devices'}

    def db_for_read(self, model, **hint):
        if model._meta.app_label == 'devices':
            return 'CTS_ER_BD'
        return None

    def db_for_rite(self, model, **hint):
        if model._meta.app_label == 'devices':
            return 'CTS_ER_BD'
        return None

    def allow_relation(self, obj1, obj2, **hint):
        if (
            obj1._meta.app_label == 'devices' or
            obj2._meta.app_label == 'devices'
        ):
            return True
        return None

    def allow_migrates(self, db, app_label, model_name=None, **hints):
        if app_label == 'devices':
            return db == 'CTS_ER_BD'
        return None