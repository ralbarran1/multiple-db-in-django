from django.db import models


class AuthUsers(models.Model):
    user_name = models.CharField(max_length=50)

    class Meta:
        managed = True
        db_table = 'auth_users'

    def __str__(self):
        return f'{self.user_name}'


class DeviceTypes(models.Model):
    device_name = models.CharField(max_length=50)
    client_ref = models.CharField(max_length=100)
    colway_ref = models.CharField(max_length=100)
    power_dis = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f'{self.colway_ref}', {self.device_name}

    class Meta:
        managed = True
        db_table = 'device_types'


class Devices(models.Model):
    device_type = models.ForeignKey(DeviceTypes, models.DO_NOTHING, db_column='device_type', blank=True, null=True)
    id_project = models.ForeignKey('Projects', models.DO_NOTHING, db_column='id_project')
    serial_number = models.IntegerField()
    manufacture_date = models.DateTimeField(blank=True, null=True)
    devices_tag = models.ImageField(blank=True, null=True)

    def __str__(self):
        return f'{self.serial_number}'

    class Meta:
        managed = True
        db_table = 'devices'


class FirmwareVers(models.Model):
    firmware_name = models.CharField(max_length=50, blank=True, null=True)
    manuf_type = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return f'{self.firmware_name}'

    class Meta:
        managed = True
        db_table = 'firmware_vers'


class HardwareVers(models.Model):
    hw_type = models.CharField(db_column='HW_type', max_length=1, blank=True, null=True)  # Field name made lowercase.

    def __str__(self):
        return f'{self.hw_type}'

    class Meta:
        managed = True
        db_table = 'hardware_vers'


class Historicals(models.Model):
    user = models.ForeignKey(AuthUsers, models.DO_NOTHING, blank=True, null=True)
    device = models.ForeignKey(Devices, models.DO_NOTHING, blank=True, null=True)
    test = models.ForeignKey('Tests', models.DO_NOTHING, blank=True, null=True)
    metadata = models.ForeignKey('Metadata', models.DO_NOTHING, blank=True, null=True)
    test_date = models.DateTimeField(blank=True, null=True)
    reuslt = models.BooleanField(blank=True, null=True)

    def __str__(self):
        return f'{self.user}, {self.device},{self.test}, {self.metadata}, {self.test_date}, {self.reuslt}'

    class Meta:
        managed = True
        db_table = 'historicals'


class Metadata(models.Model):
    id_hw_vers = models.ForeignKey(HardwareVers, models.DO_NOTHING,
                                   db_column='id_HW_vers')  # Field name made lowercase.
    id_sw_vers = models.ForeignKey('SoftwareVers', models.DO_NOTHING,
                                   db_column='id_SW_vers')  # Field name made lowercase.
    id_firmware_vers = models.ForeignKey(FirmwareVers, models.DO_NOTHING, db_column='id_firmware_vers')

    def __str__(self):
        return f'{self.id_hw_vers}, {self.id_sw_vers}, {self.id_firmware_vers}'

    class Meta:
        managed = True
        db_table = 'metadata'


class Projects(models.Model):
    project_name = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return f'{self.project_name}'

    class Meta:
        managed = True
        db_table = 'projects'


class SoftwareVers(models.Model):
    colway_ref = models.CharField(max_length=100, blank=True, null=True)
    joifi_ref = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return f'{self.colway_ref}, {self.joifi_ref}'

    class Meta:
        managed = True
        db_table = 'software_vers'


class Tests(models.Model):
    test_name = models.CharField(max_length=50, blank=True, null=True)
    prefix = models.CharField(max_length=1, blank=True, null=True)

    def __str__(self):
        return f'{self.test_name}'

    class Meta:
        managed = True
        db_table = 'tests'
