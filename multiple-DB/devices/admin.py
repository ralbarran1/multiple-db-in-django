from django.contrib import admin

from .models import Devices, Metadata, DeviceTypes, Historicals, HardwareVers, SoftwareVers, FirmwareVers, Tests, Projects, AuthUsers


@admin.register(Devices)
class DevicesAdmin(admin.ModelAdmin):
    list_display = ('id', 'device_type')
    search_fields = ("device_name__startswith", )


@admin.register(DeviceTypes)
class DeviceTypesAdmin(admin.ModelAdmin):
    list_display = ('client_ref', 'colway_ref', 'power_dis')


@admin.register(Historicals)
class HistoricalsAdmin(admin.ModelAdmin):
    list_display = ('user', 'device', 'test', 'metadata', 'test_date', 'reuslt')


@admin.register(Metadata)
class MetadataAdmin(admin.ModelAdmin):
    list_display = ('id_hw_vers', 'id_sw_vers', 'id_firmware_vers')


@admin.register(HardwareVers)
class HardwareVersAdmin(admin.ModelAdmin):
    list_display = ('id', 'hw_type')


@admin.register(SoftwareVers)
class SoftwareVersAdmin(admin.ModelAdmin):
    list_display = ('colway_ref', 'joifi_ref')


@admin.register(Tests)
class TestsAdmin(admin.ModelAdmin):
    list_display = ('test_name', 'prefix')


@admin.register(Projects)
class ProjectsAdmin(admin.ModelAdmin):
    list_display = ('id', 'project_name')


@admin.register(AuthUsers)
class AuthUsersAdmin(admin.ModelAdmin):
    list_display = ('id', 'user_name')


@admin.register(FirmwareVers)
class FirmwareVersAdmin(admin.ModelAdmin):
    list_display = ('firmware_name', 'manuf_type')

